/**
 * @author  Pablo Narvaja
 * @date    24/03/22
 * @license gpl3
*/
package common

import "core:log"
import "core:runtime"
import "core:fmt"
import "core:os"

/**
 * Ment to only be called in main procedure
*/
init_logger :: proc(file_path, name: string) -> runtime.Context {
	fd, err := os.open(
		file_path,
		os.O_WRONLY | os.O_CREATE | os.O_TRUNC,
		os.S_IRUSR | os.S_IWUSR | os.S_IRGRP | os.S_IWGRP,
	)
	if err != os.ERROR_NONE {
		fmt.printf("[FATAL] --- [Llamathrust Engine] Error creating/open file '{}'", file_path)
		os.exit(-1)
	}
	when ODIN_DEBUG {
		console_logger := log.create_console_logger(lowest = .Debug, ident = name)
		file_logger := log.create_file_logger(h = fd, lowest = .Debug, ident = name)
	} else {
		console_logger := log.create_console_logger(.Warning, {.Level} | log.Full_Timestamp_Opts, name)
		file_logger := log.create_file_logger(fd, .Info, {.Level} | log.Full_Timestamp_Opts, name)
	}
	logger = log.create_multi_logger(console_logger, file_logger)
	context.logger = logger
	context.assertion_failure_proc = assertion_failure_proc
	return context
}

/**
 * Ment to only be called in main procedure
*/
deinit_logger :: proc() {
	multi_logger := (^log.Multi_Logger_Data)(logger.data)
	for logg in &multi_logger.loggers {
		is_file_logger := logg.procedure == log.file_console_logger_proc
		if is_file_logger do log.destroy_file_logger(&logg)
	}
	log.destroy_multi_logger(&logger)
}

/**
 * This should be called within a contextless procedure
*/
get_logger :: proc "contextless" () -> runtime.Context {
	context = runtime.default_context()
	context.logger = logger
	context.assertion_failure_proc = assertion_failure_proc
	return context
}

@(private = "file")
logger: log.Logger

@(private = "file")
assertion_failure_proc :: proc(
	prefix,
	message: string,
	loc: runtime.Source_Code_Location,
) -> ! {
	if len(message) > 0 {
		fmt.printf("%v {}: {}\n", loc, prefix, message)
	} else {
		fmt.printf("%v {}\n", loc, prefix)
	}
	os.exit(-1)
}
