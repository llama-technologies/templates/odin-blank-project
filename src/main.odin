/**
 * @author  First Last Nickname
 * @date    dd/MM/yy
 * @license gpl3
*/
package main

import "common"
import "core:log"

APP_NAME :: "My App"

main :: proc() {
  context = common.init_logger(APP_NAME + ".log", APP_NAME)
  defer common.deinit_logger()

  /* your code goes here */
  log.error("Hello world!")
}
