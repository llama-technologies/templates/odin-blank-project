#!/bin/bash
# This script sets up the development environment

ERROR="\033[0;31m[ERROR] "
INFO="\033[0;32m[INFO] "
TRACE="\033[0;34m[TRACE]"
YELLOW='\033[0;33m'
ENDL='\033[0m' # No Color

ODIN_PACKAGE_NAME=odin-ubuntu-amd64-dev.*zip
DOWNLOADED_FILE=odin-ubuntu-amd64-dev*.zip
PROJECT_DEPENDENCIES="" # example "libgtk-4-bin libgtk-4-common libgtk-4-dev"

function ask_yes_or_no {
  msg=$1
  while true; do
    echo -e "${YELLOW}${msg} (y/n)?${ENDL}"
    read choice
      case "$choice" in
        y|Y ) $2
              break
              ;;
        n|N ) break
              ;;
        *   ) echo "invalid input"
            ;;
      esac
    done
}

VERSION_REGEX=([0-9]{4}[\-][0-9]{2})
installed_version=""
last_version=""

function get_last_version_string {
  local url=$1
  [[ "${url}" =~ $VERSION_REGEX ]]
  local version=$BASH_REMATCH
  echo -e "${TRACE}last version is: ${version}${ENDL}"
  last_version="${version}"
}

function get_installed_version {
  local version_msg=`odin version`
  [[ "${version_msg}" =~ $VERSION_REGEX ]]
  local version=$BASH_REMATCH
  echo -e "${TRACE}installed version is: ${version}${ENDL}"
  installed_version="${version}"
}

function has_last_version_downloaded {
  for FILE in $DOWNLOADED_FILE; do
    [[ "${FILE}" =~ $VERSION_REGEX ]]
    local version=${BASH_REMATCH[1]}
    echo -e "${TRACE}LOCAL_VERSION_FOUND: ${version}${ENDL}"
    local last_version=$1
    if [[ version == last_version ]]; then
      return 1
    fi
  done

  return 0
}

function install_odin_latest_verision {
  echo -e "${TRACE}Attempting to install odin's dependencies${ENDL}"
  sudo apt -qq install clang-13 llvm-13

  echo -e "${TRACE}Attempting to install installer's dependencies${ENDL}"
  sudo apt -qq install unzip

  echo -e "${TRACE}Unzipping...${ENDL}"
  # FIX: unzip the folder inside as "odin"
  unzip -j -q -o $DOWNLOADED_FILE 'ubuntu_artifacts/' -d odin

  if [ "$?" -eq "0" ]; then
    echo -e "${INFO}Odin unziped correctly${ENDL}"
    # FIX: sudo cp -Rf ubuntu_artifacts/ /usr/share/odin/
    sudo cp -Rf ubuntu_artifacts/ /usr/share/odin/
    sudo chmod +x /usr/share/odin/odin
    sudo ln -sf /usr/share/odin/odin /usr/bin/odin
  else
    echo -e "${ERROR}Odin unziped incorrectly"
  fi
}

function download_latest_version {
  echo -e "${TRACE}Downloading odin...${ENDL}"
  local uri=$1
  wget -nv $uri
}

function download_odin_and_install_odin {
  local url=`curl -s https://github.com/odin-lang/Odin/releases/latest -L | grep odin-ubuntu.*zip | grep href | cut -d "\"" -f 2`
  local uri="https://github.com${url}"

  get_last_version_string $url
  get_installed_version

  if [[ "${last_version}" == "${installed_version}" ]]; then
    echo -e "${INFO}Last verstion already installed${ENDL}"
    return
  fi

  # check if an odin.zip file alredy exists
  if ls $DOWNLOADED_FILE 1> /dev/null 2>&1; then
    # check if the odin.zip file found is last version
    has_last= has_last_version_downloaded $last_version

    # if it is last version
    if [[ $has_last -eq 1 ]]; then
      ask_yes_or_no "You already have downloaded latest odin version. Do you want to re/install it" install_odin_latest_verision
      return
    else
      #TODO: delete old ZIP file
      rm -f $DOWNLOADED_FILE
      echo -e "${TRACE}Deleted old zip file${ENDL}"
      download_latest_version $uri
    fi
  else
    download_latest_version $uri
  fi
  
  # install latest version
  if ls $DOWNLOADED_FILE 1> /dev/null 2>&1; then
    install_odin_latest_verision
  else
    echo -e "${ERROR}Could not download odin${ENDL}"
  fi
}

if $PROJECT_DEPENDENCIES != ""; then
  echo -e "${TRACE}Attempting to install project's dependencies${ENDL}"
  sudo apt -qq install $PROJECT_DEPENDENCIES
fi

ask_yes_or_no "Do you want to download/install odin" download_odin_and_install_odin
