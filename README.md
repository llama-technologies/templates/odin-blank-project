# File Manager
Higly configurable file manager for the LlamaOS project

## Features
- Customize the hell out of it
- Compile the customizations for a tighter and more performant version of it.
- GTK+4 GUI
- Best suitable for the LlamaOS WM
- Minimal dependencies

## Dependencies
- Odin core packages
- GTK lib

# Build
## Linux
Requires sudo.
Will install `unzip clang13 llvm13` if not installed and if odin installation choosen.
```bash
./setup.sh
```

```bash
./build.sh
```

# License
See [LICENSE](LICENSE)

# Contributing
See [CONTRIBUTING](CONTRIBUTING.md)
