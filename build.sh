#!/bin/bash
# This script builds the project binary into /bin

ERROR="\033[0;31m[ERROR] "
INFO="\033[0;32m[INFO] "
NC="\033[0m"  #NO COLOR

#############################
# Compilation script
#############################

command=$1

APP_NAME=MyApp
THREAD_COUNT=6
LINKER_FLAGS=""
DEBUG_FLAGS="-debug -o:minimal"
RELEASE_FLAGS="-o:speed -no-bounds-check -disable-assert -vet"

# -ignore-unknown-attributes
#    Ignores unknown attributes
#    This can be used with metaprogramming tools
COMMON_FLAGS="-collection:lib=./lib -build-mode:exe -thread-count:${THREAD_COUNT} -strict-style -out:./bin/${APP_NAME}"

function print_usage {
  echo -e "${NC}"
  echo -e "./build.sh [OPTION]"
  echo -e
  echo -e "OPTIONS:"
  echo -e "  -d    debug build"
  echo -e
  echo -e "${INFO} No option for release build"
  echo -e "${NC}"
}

EXTRA_LINKER_FLAGS=""
if [[ $LINKER_FLAGS ]]; then
  EXTRA_LINKER_FLAGS=-extra-linker-flags:"${LINKER_FLAGS}"
fi

case $command in
  "-d")
    odin build ./src $COMMON_FLAGS $DEBUG_FLAGS $EXTRA_LINKER_FLAGS
  ;;
  "")
    odin build ./src $COMMON_FLAGS $RELEASE_FLAGS $EXTRA_LINKER_FLAGS
  ;;
  *)
    echo -e "${ERROR}Wrong command: \"${command}\""
    print_usage
  ;;
esac
